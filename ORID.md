O:

This morning, code reviewed yesterday's homework, and the teacher gave detailed explanation and guidance. I mainly explained the use of react and redux, and then the teacher taught us how to use redux to transform the code, and then gave us a lot of practice time to transform yesterday's homework by ourselves and introduced new requirements for practice.

R:

Today's learning is very difficult, but the introduction of redux today solves the problem of complex and difficult data transfer in yesterday's assignment.

I:

redux decouples shared data as global data, which simplifies the difficulty of transferring values between react components.

D:

Apply react knowledge, do more exercises; Prepare and finish homework carefully.