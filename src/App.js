/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-24 19:31:39
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-24 19:55:20
 * @Description: file content
 * @FilePath: \todo-list\src\App.js
 */
import './App.css';
import TodoList from './components/TodoList';

function App() {
  return (
    <div className="App">
      <TodoList></TodoList>
    </div>
  );
}

export default App;
