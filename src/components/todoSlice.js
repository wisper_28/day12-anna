import { createSlice } from "@reduxjs/toolkit"
const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todoList: []
    },
    reducers: {
        addTodoItem: (state, action) => {
            state.todoList = [...state.todoList, action.payload];
        },
        changeTodoStatus: (state, action) => {
            let doneTodo = state.todoList.find(todo => todo.id === action.payload);
            doneTodo.done = !doneTodo.done;
        },
        deleteTodoItem: (state, action) => {
            let idArr = state.todoList.map((item) => {
                return item.id;
            })
            const index = idArr.indexOf(action.payload);
            state.todoList.splice(index, 1);
        }
    }
})

export const { addTodoItem, changeTodoStatus, deleteTodoItem } = todoSlice.actions;
export default todoSlice.reducer