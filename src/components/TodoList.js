/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-24 19:54:08
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-24 20:16:14
 * @Description: file content
 * @FilePath: \todo-list\src\components\TodoList.js
 */
import TodoGroup from "./TodoGroup";
import TodoItemGenerator from "./TodoItemGenerator";
import './../style/todolist.css'
import { useSelector } from "react-redux"

const TodoList = () => {
    const todoList = useSelector((state) => state.todoList.todoList)
    // console.log(todoList)
    return (
        <div>
            <p className="todo-list">Todo List</p>
            <TodoGroup todoList={todoList}></TodoGroup>
            <TodoItemGenerator></TodoItemGenerator>
        </div>
    )
}

export default TodoList;
