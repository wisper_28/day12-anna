/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-24 19:53:31
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-24 19:56:33
 * @Description: file content
 * @FilePath: \todo-list\src\components\TodoItem.js
 */
import { useDispatch } from 'react-redux';
import '../style/todoItem.css';
import { changeTodoStatus, deleteTodoItem } from './todoSlice';

const TodoItem = (props) => {
    const dispatch = useDispatch();
    const toggleDone = () => {
        dispatch(changeTodoStatus(props.value.id));
    }
    const deleteTodo = () => {
        dispatch(deleteTodoItem(props.value.id))
    }
    return (
        <div className="todo-item">
            
            <span onClick={toggleDone} className = {props.value.done? "done": "notDone"}>{props.value.text}</span>
            <svg onClick={deleteTodo} className="closeIcon" xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-x" viewBox="0 0 16 16">
  <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
</svg>
        </div>
    )
}

export default TodoItem;
