/*
 * @Autor: Haiting Zhao
 * @Date: 2023-07-25 20:25:59
 * @LastEditors: Haiting Zhao
 * @LastEditTime: 2023-07-25 20:47:46
 * @Description: file content
 * @FilePath: \day12-anna\promise.js
 */
const promise = new Promise(function(resolve, reject) {
  let count = 10;
//   let count = 19;
  if (count <= 10){
    resolve("Hello World!");
  } else {
    reject("No Thanks!");
  }
});
promise.then(function(value){
  console.log(value);
}).catch(function(value){
  console.log(value);
})